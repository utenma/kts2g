package kts2g.gfx.util

import java.util.Timer
import kotlin.concurrent.fixedRateTimer

class FPSTask {
    var frameRate: UInt = 0u
        private set
    private var frameCount: UInt = 0u

    private var timer: Timer? = null

   fun init(): Unit {
       timer = fixedRateTimer(
               initialDelay = 0,
               period = 1000,
               action = {
                   frameRate = frameCount
                   frameCount = 0u
               }
       )
   }

    fun count(): UInt = frameCount++

    fun stop(): Unit {
        timer?.cancel()
        timer?.purge()
    }
}