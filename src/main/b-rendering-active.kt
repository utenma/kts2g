package kts2g.gfx.main

import kts2g.gfx.util.FPSTask
import java.awt.Canvas
import java.awt.Color
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import java.util.concurrent.atomic.AtomicBoolean
import javax.swing.JFrame
import javax.swing.SwingUtilities
import kotlin.concurrent.thread

fun main() {
    val frameRateTask = FPSTask()

    SwingUtilities.invokeLater {
        JFrame().apply {
            title = "Active Rendering"
            // because the repaint is done in the active rendering thread loop
            ignoreRepaint = true
            defaultCloseOperation = JFrame.EXIT_ON_CLOSE

            val canvas = Canvas().apply {
                setSize(240, 120)
                background = Color.BLACK
                foreground = Color.WHITE
                ignoreRepaint = true
            }
            contentPane.add(canvas)

            // laid out window before creating buffer strategies
            pack()

            // for active rendering with offscreen and onscreen surfaces
            val bufferStrategy = canvas.run {
                createBufferStrategy(2)
                bufferStrategy
            }

            val running = AtomicBoolean(true)

            // custom active rendering thread
            val renderingLoop = thread {
                frameRateTask.init()
                // render loop
                while (running.get()) {
                    do {
                        // this loop ensures that the contents of the drawing buffer
                        // are consistent in case the underlying surface was recreated
                        do {
                            // Get a new graphics context every time through the loop
                            // to make sure the strategy is validated
                            // this graphics object will draw to the offscreen surface
                            bufferStrategy.drawGraphics.run {
                                clearRect(0, 0, canvas.width, canvas.height)
                                drawString("${frameRateTask.frameRate} FPS", 30, 30)
                                dispose()
                            }
                            frameRateTask.count()
                            // repeat the rendering if the drawing buffer contents were restored
                        } while (bufferStrategy.contentsRestored())
                        // performs the off/on screen surface swap
                        bufferStrategy.show()
                        // makes sure the offscreen surface is available
                    } while (bufferStrategy.contentsLost())
                }
            }

            addWindowListener(object : WindowAdapter() {
                override fun windowClosing(e: WindowEvent) {
                    running.set(false)
                    renderingLoop.join()
                    frameRateTask.stop()
                }
            })

            isVisible = true
        }
    }
}