package kts2g.gfx.main

import kts2g.gfx.util.FPSTask
import java.awt.Dimension
import java.awt.Graphics
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import javax.swing.JFrame
import javax.swing.JPanel
import javax.swing.SwingUtilities

fun main() {
    val frameRateTask = FPSTask()

    SwingUtilities.invokeLater {
        JFrame().apply {
            title = "Hello FPS"
            defaultCloseOperation = JFrame.EXIT_ON_CLOSE
            addWindowListener(object: WindowAdapter() {
                override fun windowClosing(e: WindowEvent?) {
                    frameRateTask.stop()
                }
            })

            contentPane.add(object : JPanel() {
                init {
                    preferredSize = Dimension(192, 64)
                }
                // In passive rendering, the app redraws itself in the paint() method,
                // the Swing event dispatch thread decides when to call this method.
                override fun paint(graphics: Graphics) {
                    super.paint(graphics)
                    frameRateTask.count()
                    graphics.drawString("${frameRateTask.frameRate} FPS", 30, 30)
                    repaint()
                }
            })

            frameRateTask.init()

            pack()
            isVisible = true
        }
    }
}